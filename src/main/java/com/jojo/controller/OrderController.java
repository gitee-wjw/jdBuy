package com.jojo.controller;

import com.jojo.entity.BasicInfo;
import com.jojo.entity.Result;
import com.jojo.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("oneKeySubmitOrderPage")
    public String oneKeySubmitOrderPage() {
        return "oneKeySubmitOrderPage";
    }

    @GetMapping("timingSubmitOrderPage")
    public String timingSubmitOrderPage() {
        return "timingSubmitOrderPage";
    }

    @PostMapping("oneKeySubmitOrder")
    @ResponseBody
    public Result oneKeySubmitOrder(@RequestBody BasicInfo basicInfo) {
        try {
            return orderService.oneKeySubmitOrder(basicInfo);
        } catch (Exception e) {
            return Result.error(-1, "下单失败");
        }
    }

    @PostMapping("timingSubmitOrder")
    @ResponseBody
    public Result timingSubmitOrder(@RequestBody BasicInfo basicInfo) {
        try {
            return orderService.timingSubmitOrder(basicInfo);
        } catch (Exception e) {
            return Result.error(-1, "设定失败");
        }
    }
}
