package com.jojo.utils;

import com.jojo.entity.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;

@Slf4j
public class HttpClientUtil {

    public static Result httpClient(String pid, String cookie) throws Exception {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(Constant.cartApi + "?pid=" + pid + "&pcount=1&ptype=1");
        httpGet.addHeader("Cookie", cookie);
        CloseableHttpResponse response = httpClient.execute(httpGet);
        String html = Jsoup.parse(EntityUtils.toString(response.getEntity())).toString();
        if (html.contains("京东-欢迎登录")) {
            return Result.error(-1, "cookie已失效");
        }
        if (html.contains("京东购物车-加购成功页")) {
            httpGet = new HttpGet(Constant.simulateApi);
            httpGet.addHeader("Cookie", cookie);
            httpGet.addHeader("Referer", "https://item.jd.com/");
            response = httpClient.execute(httpGet);
            html = Jsoup.parse(EntityUtils.toString(response.getEntity())).toString();
            if (html.contains("京东-欢迎登录")) {
                return Result.error(-1, "cookie已失效");
            }
            if (html.contains("订单结算页")) {
                httpGet = new HttpGet(Constant.submitApi);
                httpGet.addHeader("Cookie", cookie);
                httpGet.addHeader("Referer", Constant.submitApi);
                response = httpClient.execute(httpGet);
                html = Jsoup.parse(EntityUtils.toString(response.getEntity())).toString();
                if (html.contains("京东-欢迎登录")) {
                    return Result.error(-1, "cookie已失效");
                }
                return Result.success(0, "下单成功");
            } else {
                return Result.error(-1, "未知错误，请联系管理员");
            }
        } else {
            return Result.error(-1, "未知错误，请联系管理员");
        }
    }

    public static void httpClient(String cookie) throws Exception {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(Constant.chooseApi);
        httpGet.addHeader("Cookie", cookie);
        httpGet.addHeader("Referer", "https://cart.jd.com/");
        httpClient.execute(httpGet);
        httpClient.execute(httpGet);
        httpClient.execute(httpGet);
        httpGet = new HttpGet(Constant.simulateApi);
        httpGet.addHeader("Cookie", cookie);
        httpGet.addHeader("Referer", "https://item.jd.com/");
        CloseableHttpResponse response = httpClient.execute(httpGet);
        String html = Jsoup.parse(EntityUtils.toString(response.getEntity())).toString();
        if (html.contains("订单结算页")) {
            httpGet = new HttpGet(Constant.submitApi);
            httpGet.addHeader("Cookie", cookie);
            httpGet.addHeader("Referer", Constant.submitApi);
            httpClient.execute(httpGet);
        }
    }

    public static boolean checkoutCookie(String cookie) throws Exception {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(Constant.personageApi);
        httpGet.addHeader("Cookie", cookie);
        CloseableHttpResponse response = httpClient.execute(httpGet);
        String html = Jsoup.parse(EntityUtils.toString(response.getEntity())).toString();
        return html.contains("京东-欢迎登录");
    }
}
