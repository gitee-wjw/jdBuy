package com.jojo.utils;

public class Constant {
    public final static String cartApi = "https://cart.jd.com/gate.action";
    public final static String chooseApi = "https://api.m.jd.com/api?functionId=pcCart_jc_cartCheckAll&appid=JDC_mall_cart";
    public final static String simulateApi = "https://trade.jd.com/shopping/order/getOrderInfo.action";
    public final static String submitApi = "https://trade.jd.com/shopping/order/submitOrder.action";
    public final static String personageApi = "https://home.jd.com/";
}
