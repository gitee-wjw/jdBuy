package com.jojo.utils;

public class PidUtil {
    public static String parsePid(String pid) {
        if (pid.contains("product")) {
            return pid.split("product/")[1].split("\\.")[0];
        }
        if (pid.contains("com")) {
            return pid.split("com/")[1].split("\\.")[0];
        }
        return pid;
    }
}
