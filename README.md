# jdBuy京东 一键下单 定时抢购

##### 喜欢就点个star⭐吧

#### 概述

纯属研究学习，请勿商用。

#### 体验网址

[点击体验](http://139.9.67.80:1111)（手机端体验更佳）

本人承诺，不会泄露体验网址收集到的数据，也不会将这些数据用于任何场景，为保证安全，请使用虚拟手机号
#### 介绍

一、主要研究京东商品下单所需要调用的接口，只要知道cookie和商品链接即可完成下单

获取cookie方式：登录京东，f12进入开发者模式，刷新首页

![首页](https://images.gitee.com/uploads/images/2021/1026/173346_24875ac6_8919653.png "屏幕截图.png")

cookie就在请求头里， 通常以__jdu= 或 __jdv=开头

![cookie](https://images.gitee.com/uploads/images/2021/1026/173436_5c7349ba_8919653.png "屏幕截图.png")

二、加入手机号和cookie绑定，只需要输入手机号即可（第一次使用需要绑定cookie）

三、定时抢购已上线，前提是商品预约，并自动加入购物车

但无法保证百分百抢购成功，可以运行在自己本地，优化一下线程试试，欢迎大佬踊跃提交代码，希望能够给予作者更多思路

#### 技术框架

springboot2.5.5+mybatisPlus3.4.3.2

#### 运行环境

jdk1.8+mysql8.0

#### 截图
![更新cookie](https://images.gitee.com/uploads/images/2021/1026/174043_711e26da_8919653.png "屏幕截图.png")

![一键下单](https://images.gitee.com/uploads/images/2021/1026/174122_c71e24ad_8919653.png "屏幕截图.png")

![查看待付款](https://images.gitee.com/uploads/images/2021/1026/174155_45a4e026_8919653.png "屏幕截图.png")